import React, { Component } from 'react';
import classnames from 'classnames';
import styles from './App.scss';
import boyImg from './img/road.jpg';
import downloadIcon from './img/downloads-icon.png';
import fruitImg from './img/fruit.jpg';

export default class App extends Component {
    render() {
        return (
            <div class={styles.container}>
                <div class={`${styles.imgArea} ${styles.slides}`}>
                    <img class={styles.img} src={boyImg} alt="boy"/>
                    <div class={styles.overlay}>
                        <h4>Hello World</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi tempora quasi, perferendis cumque facilis. Nostrum provident minus maxime, similique ?</p>
                        <div class={styles.downloadBtn}>DOWNLOAD
                            <img class={styles.downloadIcon} src={downloadIcon} alt=""/>
                        </div>
                    </div>
                </div>
                <div class={classnames(styles.imgArea, styles.cubicBezier)}>
                    <img class={styles.img} src={fruitImg} alt=""/>
                </div>
            </div>
        );
    }
}